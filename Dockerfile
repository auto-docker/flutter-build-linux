FROM ubuntu:24.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
  git \
  curl \
  unzip \
  clang \
  cmake \
  ninja-build \
  pkg-config \
  libgtk-3-dev \
  liblzma-dev \
  xz-utils \
  && rm -rf /var/lib/apt/lists/*


WORKDIR /root
RUN curl 'https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.24.1-stable.tar.xz' > flutter_linux.tar.xz \
  && tar xf flutter_linux.tar.xz \
  && rm flutter_linux.tar.xz

RUN git config --global --add safe.directory /root/flutter

ENV PATH="$PATH:/root/flutter/bin"

RUN flutter --disable-analytics \
  && flutter config --no-enable-macos-desktop \
  && flutter config --no-enable-windows-desktop \
  && flutter config --no-enable-android \
  && flutter config --no-enable-ios \
  && flutter config --no-enable-fuchsia \
  && flutter config --no-enable-custom-devices \
  && flutter config --enable-linux-desktop \
  && flutter precache --linux --universal
