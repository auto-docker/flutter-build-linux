#!/bin/sh

rm -rf public/*

PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE apt list --manual-installed` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE apk list -I` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE dpkg -l` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE yum list installed` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE pacman -Q` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE dnf list --installed` \
 || PACKAGE_LIST=`docker run --rm $CI_REGISTRY_IMAGE opkg list-installed` \
 || PACKAGE_LIST="Could not get package list
Package manager not supported, please report this at
https://gitlab.com/robblue2x-docker-images/example/issues
remember to report which base docker image you're using"

which envsubst || apk add gettext

mkdir -p public

README=`cat README.md` \
DATE=`date` \
PACKAGE_LIST="$PACKAGE_LIST" \
envsubst < template.html > public/index.html
