module.exports = {
  branches: ['master'],
  plugins: ['@semantic-release/commit-analyzer', '@semantic-release/gitlab'],
  tagFormat: '${version}',
};
